package com.epam.java.task3.example.SortBy;

import com.epam.java.task3.example.Enum.TypeOfCandy;
import static com.epam.java.task3.example.Enum.TypeOfCandy.*;

public class SortByParameter {
    public static void Sorted(int wantedSugar) {
        TypeOfCandy candy = TypeOfCandy.valueOf(String.valueOf(CANDY));
        TypeOfCandy chocolate = TypeOfCandy.valueOf(String.valueOf(CHOCOLATE));
        TypeOfCandy cookie = TypeOfCandy.valueOf(String.valueOf(COOKIE));
        TypeOfCandy dough = TypeOfCandy.valueOf(String.valueOf(Dough));

        System.out.print("По кількості цукру найкраще підходить: ");
        if(wantedSugar >= cookie.getSugar() && wantedSugar <= dough.getSugar()){
            System.out.println(Dough);
        }else {
            if (wantedSugar >= chocolate.getSugar() && wantedSugar <= cookie.getSugar()) {
                System.out.print(COOKIE);
            }else {
                if (wantedSugar >= candy.getSugar() && wantedSugar <= chocolate.getSugar()) {
                    System.out.print(CHOCOLATE);
                }else {
                    if (wantedSugar > 0 && wantedSugar <= candy.getSugar()) {
                        System.out.print(CANDY);
                    }
                }
            }
        }
    }
}