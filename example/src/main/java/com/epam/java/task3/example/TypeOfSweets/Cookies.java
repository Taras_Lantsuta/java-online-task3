package com.epam.java.task3.example.TypeOfSweets;

import com.epam.java.task3.example.Sweets;
import com.epam.java.task3.example.Enum.TypeOfCandy;

public class Cookies extends Sweets {
    private final static String name = "Печеньки";

    public Cookies(double weight, TypeOfCandy typeOfCandy) {
        super(weight, name, typeOfCandy);
    }
}