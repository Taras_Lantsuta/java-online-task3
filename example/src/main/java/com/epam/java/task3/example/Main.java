package com.epam.java.task3.example;

import com.epam.java.task3.example.TypeOfSweets.*;
import com.epam.java.task3.example.SortBy.SortByParameter;
import static com.epam.java.task3.example.Enum.TypeOfCandy.*;

public class Main {
    public static void main(String[] args) {
        Cookies cookie1 = new Cookies(36.5 , COOKIE);     //weight and type of sweets
        Candys candy1 = new Candys(20 , CANDY);
        Candys candy2 = new Candys(42.42 , CANDY);
        int wantedSugar = 10;
        Gift gift = new Gift();
        gift.addSweet(cookie1).addSweet(candy1).addSweet(candy2);
        System.out.println(gift.getWeight());
        SortByParameter.Sorted(wantedSugar);
    }
}