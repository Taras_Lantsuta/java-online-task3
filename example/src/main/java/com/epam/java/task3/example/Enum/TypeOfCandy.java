package com.epam.java.task3.example.Enum;

public enum TypeOfCandy {
    COOKIE(100),
    CANDY(50),
    CHOCOLATE(75),
    Dough(125);
    private int sugar;

    TypeOfCandy(int sugar) {
        this.sugar = sugar;
    }

    public int getSugar() {
        return this.sugar;
    }
}