package com.epam.java.task3.example;

import com.epam.java.task3.example.Enum.TypeOfCandy;

public class Sweets {
    private final double weight;
    private final String name;
    private final TypeOfCandy typeOfCandy;

    protected Sweets(double weight, String name, TypeOfCandy typeOfCandy) {
        if (weight < 0) {
            throw new IllegalArgumentException("Weight can not be less then zero.");
        }
        this.weight = weight;
        if (name == null) {
            throw new NullPointerException("Argument name can not be null.");
        }
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Name of candy can not be empty.");
        }
        this.name = name;
        if (typeOfCandy == null) {
            throw new NullPointerException("Argument typeOfCandy can not be null.");
        }
        this.typeOfCandy = typeOfCandy;
    }

    public double getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }

    public TypeOfCandy getTypeOfCandy() {
        return typeOfCandy;
    }
}

