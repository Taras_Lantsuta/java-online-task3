package com.epam.java.task3.example;

import java.util.ArrayList;
import java.util.List;

public class Gift {
    private double weight;
    private List<Sweets> sweets = new ArrayList<Sweets>();

    public double getWeight() {
        System.out.print("Подарунок важить: ");
        return weight;
    }

    public Gift addSweet(Sweets sweet) {
        sweets.add(sweet);
        weight += sweet.getWeight();
        return this;
    }
}
